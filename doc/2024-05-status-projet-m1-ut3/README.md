# Status du Projet de M1 UT3 2024
Ce dossier tient compte du travail effectué lors de l'UE Projet 2024 par les étudiants de l'Université Paul Sabatier.

## Floppy Team
Le projet proposé par la fondation FLOPEDT a été géré par la Floppy Team, composée de
- Cynthia Rey, Dev. Lead & Git Mistress
- Adam Pouplard, Scrum Master
- André Martins-Capitao, Product Owner
- Gabriel Moltot, Developer & Documentation Lead
- Arthur Barrière, Developer & Lead UI/UX

## Travail réalisé
### Conception de prototypes moyenne fidélité
Dans une optique de mettre en place une conception centrée utilisateur, des prototypes moyenne fidélité ont été
créés afin de mieux diriger l'équipe vers quoi travailler.

La Floppy Team a utilisé Balsamiq, car outil utilisé dans l'Université. Ils ont été exportés au format PDF.

- [Prototype moyenne fidélité global interface de gestion](./proto-balsamiq-global.pdf)
- [Prototype moyenne fidélité interface de création de cours](./proto-balsamiq-creation.pdf)

### Interface de visualisation des cours
Cette interface permet de visualiser l'ensemble des cours pour un département.
Elle comporte un système de filtres et de tri à gauche, et un aperçu des cours à droite.
La sélection multiple est possible à l'aide de raccourcis claviers.

- [./screenshot-interface-cours-base.png](./screenshot-interface-cours-base.png) : Interface de visualisation des cours de base
- [./screenshot-interface-cours-filtre1.png](./screenshot-interface-cours-filtre1.png) : Visualisation avec un filtre
- [./screenshot-interface-cours-filtre3.png](./screenshot-interface-cours-filtre3.png) : Visualisation avec des filtres empilés
- [./screenshot-interface-cours-select1.png](./screenshot-interface-cours-select1.png) : Visualisation des détails d'un cours
- [./screenshot-interface-cours-selectN.png](./screenshot-interface-cours-selectN.png) : Visualisation des détails de plusieurs cours

#### Pistes d'amélioration
- Permettre de replier les colonnes sur les côtés pour augmenter la visibilité
- Ajout d'une légende pour les couleurs des blocs
- Vérifier l'accessibilité de l'interface (WCAG 2.0)
- Performances (voir [Pistes d'améliorations notables](#pistes-daméliorations-notables))

### Interface de modification des emplois du temps
Cette interface, "intégrée" au panneau de visualisation de détails permet de modifier un (ou plusieurs) cours existant.
Lors d'une édition multiple, les champs qui ne sont pas les mêmes pour chaque cours sont protégés, mais il est possible
de les éditer quand même ce qui écrasera toutes les valeurs.

- [./screenshot-interface-cours-modif1.png](./screenshot-interface-cours-modif1.png) : Interface de modification d'un cours
- [./screenshot-interface-cours-modifN.png](./screenshot-interface-cours-modifN.png) : Interface de modification de plusieurs cours

#### Pistes d'amélioration
- Améliorer la gestion des durées (champ spécifique à ce type de données dans l'UIKit)
- Ajouter des attributs supplémentaires, non gérés ici où manquant dans la base de données (`module_supp`, `is_graded`, `comment`, etc)
- Ajouter un affichage des cours avant validation (Avant Après)
- Vérifier l'accessibilité de l'interface (WCAG 2.0)
- Améliorer l'UIKit et son utilisation (utilisation de `XxxxField` au lieu des champs en dur)
- Vérifier la robustesse via des tests E2E (voir [Pistes d'améliorations notables](#pistes-daméliorations-notables))

### Interface de suppression de cours
Peu de choses à dire, elle sert de confirmation avant de supprimer des cours.

- [./screenshot-interface-cours-suppr.png](./screenshot-interface-cours-suppr.png) : Boîte de dialogue de confirmation

#### Pistes d'amélioration
- ~~Il serait souhaitable d'ajouter un toast qui confirme la suppression des éléments sélectionnés.~~ *Traité lors du nettoyage du projet pré-merge*

### Interface de création de cours
Cette interface permet de créer un ou plusieurs cours. La création simple est triviale, et la création multiple a été
découpée en 3 phases afin de répondre au mieux aux demandes des utilisateurs :
- Étape 1 : Propriétés globales - celles-ci seront communes à tous les cours créés.
- Étape 2 : Propriétés divergentes - les propriétés précédemment sélectionnées ne sont plus modifiables.
            Pour chaque propriété restante, il est possible de saisir plusieurs valeurs. Les cours créés seront le
            résultat du produit cartésien des valeurs sélectionnées.
- Étape 3 : Visualisation globale des cours qui vont être créés avec possibilité de faire des retouches manuelles.
            *Cette étape n'a pas pu être implémentée à ce stade*

- [./screenshot-interface-cours-create1.png](./screenshot-interface-cours-create1.png) : Création d'un cours
- [./screenshot-interface-cours-createN-pt1.png](./screenshot-interface-cours-createN-pt1.png) : Création de plusieurs cours (étape 1)
- [./screenshot-interface-cours-createN-pt2.png](./screenshot-interface-cours-createN-pt2.png) : Création de plusieurs cours (étape 2)
  - Tous les champs sont de type `TagsInput`, ce qui permet de sélectionner plusieurs valeurs facilement.

#### Pistes d'amélioration
- Vérifier l'accessibilité de l'interface (WCAG 2.0)
- Implémenter l'étape 3 du processus de création de cours
- Améliorer le design de la boîte de dialogue, éventuellement mieux guider les utilisateurs dans le processus
- Effectuer des tests d'utilisabilité, le composant devant répondre a de nombreuses demandes de façon claire
- Améliorer l'UIKit et son utilisation (utilisation de `XxxxField` au lieu des champs en dur)
- Vérifier la robustesse via des tests E2E (voir [Pistes d'améliorations notables](#pistes-daméliorations-notables))

## Chantiers restants
Il existe encore des aspects du projet qui n'ont pas été traités par la Floppy Team en conséquence d'un manque de temps
et d'imprévus lors du projet.

### Visualisations des contraintes de précédence (dépendances)
Cette interface sous forme d'un "organigramme" (flowchart) a pour but de fournir un aperçu visuel des contraintes
de précédence.

Le prototype Balsamiq (global) fourni contient le prototype de cet affichage.

#### Pistes d'implémentation
Nous avions pour projet d'utiliser Vue Flow afin d'implémenter cette interface. C'est une bibliothèque robuste qui
permet de faire ce type de composant intéractif de façon relativement simple. https://vueflow.dev/

## Pistes d'améliorations notables
Nous avons noté ces pistes lors du projet, qui sont des élements que nous n'avons pas eu le temps de traiter ou des
élements en dehors du cadre dans lequel nous travaillons.

Nous pouvons noter :
- Le manque de tests de bout-en-bout de l'application. L'outil Playwright a été mis en place et configuré afin de
  permettre leur mise en place, mais aucun test n'a été créé avec l'outil.
- Le début d'introduction d'un "UIKit" et de variables CSS pour les couleurs. Cet UIKit est actuellement peu fourni et
  beaucoup d'élements manquent à l'appel.
  - Les palettes de gris sont possiblement à revoir et/ou à étendre.
  - Cet UIKit a commencé la mise en place d'une compatibilité thème sombre/thème clair.
  - Certains élements sont peu (ou même pas) documentés...
- Certaines entrées API souffrent de problèmes N+1 requêtes SQL ce qui nuit à la performance de l'application.
  - Certaines des entrées ont été corrigées, mais il serait intéressant de se pencher en détail sur ce problème qui
    une fois corrigé peut offrir des gains de performance considérables.
- ~~Les performances de l'affichage des cours sont plutôt médiocres.~~ *Traité lors du nettoyage du projet pré-merge*
  - ~~Aucune bibliothèque de lazy scrolling qui permette de gérer ce problème étant disponibles, la conception d'une
  	solution "maison" semble nécessaire malheureusement.~~
