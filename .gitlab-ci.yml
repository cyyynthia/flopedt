workflow:
  auto_cancel:
    on_new_commit: interruptible
  rules:
    # Only run CI on protected branches and on merge request
    # Saves on runner resources as branches in development don't need CI and will go through a merge request.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'

stages:
  - build
  - lint
  - test
  - deploy

cache:
  - key:
      files:
        - requirements.txt
    paths:
      - .cache/pip
  - key:
      files:
        - FlOpEDT-front/yarn.lock
    paths:
      - FlOpEDT-front/node_modules

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  DJANGO_SETTINGS_MODULE: "FlOpEDT.settings.base"
  FLOP_CONFIG_FILE: "$CI_PROJECT_DIR/config_file/flopedt-ci.ini"
  POSTGRES_DB: "postgres"
  POSTGRES_HOST: "postgres"
  POSTGRES_PORT: "5432"
  POSTGRES_USER: "flop_user"
  POSTGRES_PASSWORD: "flop_pwd"
  POSTGRES_HOST_AUTH_METHOD: "trust"

.python:
  image: python:3.11-alpine
  services:
    - postgres:alpine
  before_script:
    - python3.11 --version # For debugging
    - python3.11 -m venv venv --without-pip --system-site-packages
    - source venv/bin/activate
    - python3 --version ; python3 -m pip --version # For debugging, again
    - python3 -m pip install -r requirements.txt

.python-full:
  extends: .python
  services:
    - postgres:alpine
    - redis:alpine
    - maildev/maildev:latest

.node:
  image: node:alpine
  before_script:
    - node --version ; yarn --version # For debugging
    - cd FlOpEDT-front ; yarn --frozen-lockfile ; cd ..

.playwright:
  # https://mcr.microsoft.com/en-us/product/playwright/tags
  image: mcr.microsoft.com/playwright:v1.44.0-jammy
  services:
    - postgres:alpine
  before_script:
    - echo -e "section_start:`date +%s`:python_install\r\e[0KInstallation of Python"
    # tzdata hangs on readline otherwise...
    - ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime
    # Install Python 3.11
    - apt -qq update ; apt -qq install -y software-properties-common ; add-apt-repository -y ppa:deadsnakes/ppa
    - apt -qq install -y python3.11 python3.11-distutils python3.11-venv python3-pip
    - echo -e "section_end:`date +%s`:python_install\r\e[0K"

    - echo -e "section_start:`date +%s`:python_setup\r\e[0KSetting up Python environment"
    - !reference [ .python, before_script ]
    - echo -e "section_end:`date +%s`:python_setup\r\e[0K"

    - echo -e "section_start:`date +%s`:node_setup\r\e[0KSetting up Node environment"
    # Microsoft's image comes with Node and Yarn installed
    # https://github.com/microsoft/playwright/blob/main/utils/docker/Dockerfile.jammy
    - !reference [ .node, before_script ]
    - echo -e "section_end:`date +%s`:node_setup\r\e[0K"

    - echo -e "section_start:`date +%s`:database_init\r\e[0KInitializing database"
    - cd FlOpEDT
    - python3 manage.py migrate
    - python3 manage.py loaddata ../dump.json.bz2
    - cd ..
    - echo -e "section_end:`date +%s`:database_init\r\e[0K"

include:
  - local: ".gitlab/ci/backend-ci.yml"
  - local: ".gitlab/ci/webapp-ci.yml"
  - local: ".gitlab/ci/build-ci.yml"
  # TODO merge builds, adapt deploy
  # - local: '.gitlab/ci/old-packages.yml'
