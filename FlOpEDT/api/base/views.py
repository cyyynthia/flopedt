# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.
import django_filters.rest_framework as filters
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import base.models as bm
from api.base import serializers
from api.permissions import IsAdminOrReadOnly, IsTutorOrReadOnly
from api.shared.params import dept_param, week_param, year_param

# ------------
# -- GROUPS --
# ------------


class DepartmentViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the departments

    Can be filtered as wanted with every field of a Department object.
    """

    permission_classes = [IsAdminOrReadOnly]

    queryset = bm.Department.objects.all()
    serializer_class = serializers.DepartmentSerializer


# ------------
# -- TIMING --
# ------------


class HolidaysViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the holidays

    Can be filtered as wanted with every field of a Holidays object.
    """

    queryset = bm.Holiday.objects.all()
    serializer_class = serializers.HolidaysSerializer
    filterset_fields = "__all__"


class TrainingHalfDaysViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the half-day trainings

    Can be filtered as wanted with every field of a TrainingHalfDay object.
    """

    queryset = bm.TrainingHalfDay.objects.all()
    serializer_class = serializers.TrainingHalfDaysSerializer

    filterset_fields = "__all__"


class PeriodsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the periods

    Can be filtered as wanted with every field of a Period object.
    """

    queryset = bm.TrainingPeriod.objects.all()
    serializer_class = serializers.TrainingPeriodSerializer

    filterset_fields = "__all__"


class TimeGeneralFilter(filters.FilterSet):
    days = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = bm.TimeGeneralSettings
        fields = ("department", "days")


class TimeGeneralSettingsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the settings of time

    Can be filtered as wanted with parameter="days" of a TimeGeneralSetting object by calling the function TimeGeneralFilter
    """

    queryset = bm.TimeGeneralSettings.objects.all()
    serializer_class = serializers.TimeGeneralSettingsSerializer

    filterset_class = TimeGeneralFilter


class SchedulingPeriodsFilter(filters.FilterSet):
    class Meta:
        model = bm.SchedulingPeriod
        fields = ("start_date",)


class SchedulingPeriodsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all Scheduling periods

    Can be filtered as wanted with every field of a Department object.
    """

    permission_classes = [IsAdminOrReadOnly]

    queryset = bm.SchedulingPeriod.objects.all()
    serializer_class = serializers.SchedulingPeriodsSerializer
    filterset_class = SchedulingPeriodsFilter


# -------------
# -- COURSES --
# -------------

# -----------------
# - MODIFICATIONS -
# -----------------


class CourseModificationsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the course modifications.

    Can be filtered as wanted with every field of a CourseModification object.
    """

    queryset = bm.CourseModification.objects.all()
    serializer_class = serializers.CourseModificationsSerializer

    filterset_fields = "__all__"

    permission_classes = [IsAdminOrReadOnly]


# -----------
# -- COSTS --
# -----------


class TutorCostsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the tutor costs.

    Can be filtered as wanted with every field of a TutorCost object.
    """

    queryset = bm.TutorCost.objects.all()
    serializer_class = serializers.TutorCostsSerializer

    filterset_fields = "__all__"


class GroupCostsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the group costs.

    Can be filtered as wanted with every field of a GroupCost object.
    """

    queryset = bm.GroupCost.objects.all()
    serializer_class = serializers.GroupCostsSerializer

    filterset_fields = "__all__"


class GroupFreeHalfDaysViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the group's free half days.

    Can be filtered as wanted with every field of a GroupFreeHalfDay object.
    """

    queryset = bm.GroupFreeHalfDay.objects.all()
    serializer_class = serializers.GroupFreeHalfDaysSerializer

    filterset_fields = "__all__"


# ----------
# -- MISC --
# ----------


class DependenciesViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the dependencies between courses.

    Can be filtered as wanted with every field of a Dependency object.
    """

    queryset = bm.Dependency.objects.all()
    serializer_class = serializers.DependenciesSerializer

    filterset_fields = "__all__"


class RegenFilterSet(filters.FilterSet):
    dept = filters.CharFilter(field_name="department__abbrev", required=True)

    class Meta:
        model = bm.Regen
        fields = ["dept"]


class RegensViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the regenerations.

    Can be filtered as wanted with "year"[required] / "week"[required] / "dept"[required]
    of a Regen object by calling the function RegensFilterSet
    """

    queryset = bm.Regen.objects.all()
    serializer_class = serializers.RegensSerializer
    filterset_class = RegenFilterSet


class LoginView(TemplateView):
    template_name = "login.html"
    queryset = ""
    serializer_class = serializers.LoginSerializer

    def post(self, request, **kwargs):
        username = request.POST.get("username", False)
        password = request.POST.get("password", False)
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)  # Bon URL à mettre

        return render(request, self.template_name)

    def get_extra_actions():
        return []


class LogoutView(TemplateView):
    template_name = "login.html"
    queryset = ""
    serializer_class = serializers.LogoutSerializer

    def get(self, request, **kwargs):
        logout(request)

        return render(request, self.template_name)

    def get_extra_actions():
        return []


# ---------------
# --- OTHERS ----
# ---------------


class TrainingProgrammeFilterSet(filters.FilterSet):
    dept = filters.CharFilter(field_name="department__abbrev", required=True)

    class Meta:
        model = bm.TrainingProgramme
        fields = ["dept"]


class TrainingProgrammeNameViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet to see all the training programs
    """

    permission_classes = [IsAdminOrReadOnly]
    queryset = bm.TrainingProgramme.objects.all()
    serializer_class = serializers.TrainingProgrammeNameSerializer
    filterset_class = TrainingProgrammeFilterSet


class TrainingProgrammeViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the training programs
    """

    permission_classes = [IsAdminOrReadOnly]
    queryset = bm.TrainingProgramme.objects.all()
    serializer_class = serializers.TrainingProgrammeSerializer
    filterset_class = TrainingProgrammeFilterSet


class ContactView(APIView):
    # permission_classes = [IsAuthenticated]

    def post(self, request):
        # Need to retrieve the current department
        return redirect("/contact/INFO")
