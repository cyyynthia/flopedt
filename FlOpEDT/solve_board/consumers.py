# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

# from django.http import HttpResponse
# from channels.handler import AsgiHandler
# from channels import Group, Channel
# from tasks import run

import io
import json
import logging

# from multiprocessing import Process
import os
import signal
import sys
import traceback

from channels.generic.websocket import WebsocketConsumer
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

import TTapp.models as TimetableClasses
from base.models import SchedulingPeriod, TrainingProgramme
from TTapp.timetable_model import TimetableModel


logger = logging.getLogger(__name__)


class SolverConsumer(WebsocketConsumer):
    def get_constraint_class(self, string):
        return getattr(sys.modules[TimetableClasses.__name__], string)

    def connect(self):
        # ws_message()
        self.accept()
        self.send(
            text_data=json.dumps(
                {
                    "message": "hello",
                    "action": "info",
                }
            )
        )

    def disconnect(self, code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        logger.debug(" WebSocket receive data : %s", data)

        if data["action"] == "go":
            # Save constraints state
            for constraint in data["constraints"]:
                try:
                    flop_model = self.get_constraint_class(constraint["model"])
                    instance = flop_model.objects.get(pk=constraint["pk"])
                    instance.is_active = constraint["is_active"]
                    instance.save()
                except ObjectDoesNotExist:
                    print(
                        f"unable to find {constraint['model']} for id {constraint['pk']}"
                    )
                except AttributeError:
                    print(f"error while importing {constraint['model']} model")

            # Get work copy as stabilization base
            try:
                major_to_stabilize = int(data["stabilize"])
            except:  # pylint: disable=bare-except
                major_to_stabilize = None

            # Get additional informations
            time_limit = data["time_limit"] or None
            periods = [SchedulingPeriod.objects.get(name__in=data["periods_names"])]

            # Start solver
            Solve(
                data["department"],
                periods,
                data["timestamp"],
                data["train_prog"],
                self,
                time_limit,
                data["solver"],
                data["pre_assign_rooms"],
                data["post_assign_rooms"],
                major_to_stabilize=major_to_stabilize,
                all_periods_together=data["all_periods_together"],
                send_log_email=data["send_log_email"],
                user_email=data["current_user_email"],
            ).start()

        elif data["action"] == "stop":
            solver_child_process = cache.get("solver_child_process")
            if solver_child_process:
                self.send(
                    text_data=json.dumps(
                        {
                            "message": "sending SIGINT to solver (PID:"
                            + str(solver_child_process)
                            + ")...",
                            "action": "aborted",
                        }
                    )
                )
                os.kill(solver_child_process, signal.SIGINT)
                cache.delete("solver_child_process")
            else:
                self.send(
                    text_data=json.dumps(
                        {"message": "there is no running solver!", "action": "aborted"}
                    )
                )


def solver_subprocess_signint_handler(sig, stack):  # pylint: disable=unused-argument
    # ignore in current process and forward to process group (=> gurobi)
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    os.kill(0, signal.SIGINT)


class Solve:  # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        department_abbrev,
        periods,
        timestamp,
        training_programme,
        chan,
        time_limit,
        solver,
        pre_assign_rooms,
        post_assign_rooms,
        send_log_email,
        user_email,
        major_to_stabilize=None,
        all_periods_together=True,
    ):
        super().__init__()
        self.department_abbrev = department_abbrev
        self.periods = periods
        self.timestamp = timestamp
        self.channel = chan
        self.time_limit = time_limit
        self.solver = solver
        self.major_to_stabilize = major_to_stabilize
        self.pre_assign_rooms = pre_assign_rooms
        self.post_assign_rooms = post_assign_rooms
        self.all_periods_together = all_periods_together
        if send_log_email:
            self.user_email = user_email
        else:
            self.user_email = None

        # if all train progs are called, training_programme=''
        try:
            self.training_programme = TrainingProgramme.objects.get(
                abbrev=training_programme, department__abbrev=department_abbrev
            )
        except ObjectDoesNotExist:
            self.training_programme = None

    def start(self): # pylint: disable=too-many-branches
        solver_child_process = cache.get("solver_child_process")
        if solver_child_process:
            self.channel.send(
                text_data=json.dumps(
                    {
                        "message": "another solver is currently running (PID:"
                        + str(solver_child_process)
                        + "), let's wait"
                    }
                )
            )
            return
        try:
            (rd, wd) = os.pipe()
            solver_child_process = os.fork()
            if solver_child_process == 0:
                os.dup2(wd, 1)  # redirect stdout
                os.dup2(wd, 2)  # redirect stderr
                try:
                    if self.all_periods_together:
                        t = TimetableModel(
                            self.department_abbrev,
                            periods=self.periods,
                            train_prog=self.training_programme,
                            major_to_stabilize=self.major_to_stabilize,
                            pre_assign_rooms=self.pre_assign_rooms,
                            post_assign_rooms=self.post_assign_rooms,
                        )
                        os.setpgid(os.getpid(), os.getpid())
                        signal.signal(signal.SIGINT, solver_subprocess_signint_handler)
                        t.solve(
                            time_limit=self.time_limit,
                            solver=self.solver,
                            send_gurobi_logs_email_to=self.user_email,
                        )
                    else:
                        for period in self.periods:
                            t = TimetableModel(
                                self.department_abbrev,
                                [period],
                                train_prog=self.training_programme,
                                major_to_stabilize=self.major_to_stabilize,
                                pre_assign_rooms=self.pre_assign_rooms,
                                post_assign_rooms=self.post_assign_rooms,
                            )
                            os.setpgid(os.getpid(), os.getpid())
                            signal.signal(
                                signal.SIGINT, solver_subprocess_signint_handler
                            )
                            t.solve(
                                time_limit=self.time_limit,
                                solver=self.solver,
                                send_gurobi_logs_email_to=self.user_email,
                            )
                except:  # pylint: disable=bare-except
                    traceback.print_exc()
                    print("solver aborting...")
                    os._exit(1)
                finally:
                    print("solver exiting")
                    os._exit(0)
            else:
                cache.set("solver_child_process", solver_child_process, None)
                print("starting solver sub-process " + str(solver_child_process))
                if solver_child_process != cache.get("solver_child_process"):
                    print(
                        "unable to store solver_child_process PID, cache not working?"
                    )
                    self.channel.send(
                        text_data=json.dumps(
                            {
                                "message": (
                                    "unable to store solver_child_process PID, "
                                    "you won't be able to stop it via the browser! "
                                    "Is Django cache not working?"
                                ),
                                "action": "error",
                            }
                        )
                    )
                os.close(wd)

                with io.TextIOWrapper(io.FileIO(rd)) as tube:
                    for line in tube:
                        self.channel.send(
                            text_data=json.dumps({"message": line, "action": "info"})
                        )

                while 1:
                    (child, status) = os.wait()
                    if child == solver_child_process and os.WIFEXITED(status):
                        break

                cache.delete("solver_child_process")
                if os.WEXITSTATUS(status) != 0:
                    self.channel.send(
                        text_data=json.dumps(
                            {
                                "message": (
                                    "solver process has aborted with "
                                    f"a {os.WEXITSTATUS(status)} error code"
                                ),
                                "action": "error",
                            }
                        )
                    )
                else:
                    self.channel.send(
                        text_data=json.dumps(
                            {
                                "message": "solver process has finished",
                                "action": "finished",
                            }
                        )
                    )

        except OSError:
            print("Exception while launching a solver sub-process:")
            traceback.print_exc()
            print("Continuing business as usual...")


# https://vincenttide.com/blog/1/django-channels-and-celery-example/
# http://docs.celeryproject.org/en/master/django/first-steps-with-django.html#django-first-steps
# http://docs.celeryproject.org/en/master/getting-started/next-steps.html#next-steps


# send a signal
# https://stackoverflow.com/questions/15080500/how-can-i-send-a-signal-from-a-python-program#20972299
# output file+console
# https://stackoverflow.com/questions/11325019/output-on-the-console-and-file-using-python
# redirect pulp
# https://stackoverflow.com/questions/26642029/writing-coin-or-cbc-log-file
# start function in new process
# https://stackoverflow.com/questions/7207309/python-how-can-i-run-python-functions-in-parallel#7207336
# generate uuid: import uuid ; uuid.uuid4()


# channel init
# https://blog.heroku.com/in_deep_with_django_channels_the_future_of_real_time_apps_in_django


# moving to production
# https://channels.readthedocs.io/en/1.x/backends.html
# port 6379?


# docker image
# sudo apt-get install redis-server
