from enum import Enum


class Theme(Enum):
    WHITE = "White"
    DARK = "Dark"
    SYNTH_WAVE = "SynthWave"
    BRUME = "Brume"
    PRESTIGE = "Prestige Edition"
    PINK = "Pink"
