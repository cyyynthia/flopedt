# Generated by Django 3.0.14 on 2024-02-21 11:58

import datetime as dt
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion

def fill_duration_department_and_new_times(apps, schema_editor):
    CourseStartTimeConstraint = apps.get_model('base', 'CourseStartTimeConstraint')
    for csc in CourseStartTimeConstraint.objects.all():
        if csc.course_type is None:
            csc.delete()
            continue
        duration = dt.time(hour=csc.course_type.duration//60, minute=csc.course_type.duration%60)
        csc.department = csc.course_type.department
        if CourseStartTimeConstraint.objects.filter(duration = duration).exists():
            csc.delete()
            continue
        csc.duration = duration
        csc.allowed_start_times_tmp = [dt.time(hour=ast//60, minute=ast%60) for ast in csc.allowed_start_times]
        csc.save()

class Migration(migrations.Migration):

    dependencies = [
        ('base', '0109_course_duration'),
    ]

    operations = [
        migrations.AddField(
            model_name='coursestarttimeconstraint',
            name='department',
            field=models.ForeignKey(null=True, default=None, on_delete=django.db.models.deletion.CASCADE, to='base.Department'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='coursestarttimeconstraint',
            name='duration',
            field=models.DurationField(default=dt.timedelta(seconds=3600), verbose_name='Duration'),
        ),
        migrations.AddField(
            model_name='coursestarttimeconstraint',
            name='allowed_start_times_tmp',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TimeField(), blank=True, null=True, size=None),
        ),
        migrations.RunPython(fill_duration_department_and_new_times),
        migrations.RemoveField(
            model_name='coursestarttimeconstraint',
            name='course_type',
        ),
        migrations.AddField(
            model_name='mode',
            name='scheduling_mode',
            field=models.CharField(choices=[('d', 'day'), ('w', 'week'), ('m', 'month'), ('y', 'year'), ('c', 'custom')], default='w', max_length=1),
        ),
        migrations.RemoveField(
            model_name='coursestarttimeconstraint',
            name='allowed_start_times',
        ),
        migrations.RenameField(
            model_name='coursestarttimeconstraint',
            old_name='allowed_start_times_tmp',
            new_name='allowed_start_times',
        ),
        migrations.AlterField(
            model_name='transversalgroup',
            name='parallel_groups',
            field=models.ManyToManyField(blank=True, related_name='_transversalgroup_parallel_groups_+', to='base.TransversalGroup'),
        ),
        migrations.AlterField(
            model_name='coursestarttimeconstraint',
            name='department',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='base.Department')
        ),
    ]
