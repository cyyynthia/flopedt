# Generated by Django 4.2 on 2024-03-14 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0120_remove_dependency_nd'),
        ('TTapp', '0095_rename_limitundesiredslotsperperiod_limitundesiredslotsperdayperiod'),
    ]

    operations = [
        migrations.AddField(
            model_name='breakaroundcoursetype',
            name='train_progs',
            field=models.ManyToManyField(blank=True, to='base.trainingprogramme'),
        ),
    ]
