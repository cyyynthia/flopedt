import { defineConfig } from 'vitest/config'
import { fileURLToPath } from 'url'
import svg from 'vite-plugin-magical-svg'
import vue from '@vitejs/plugin-vue'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

const FLOP_BACKEND_URL = process.env.FLOP_BACKEND_URL || 'http://127.0.0.1:8000'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svg({ target: 'vue' }),
    vue({ template: { transformAssetUrls } }),
    vueI18n({ include: fileURLToPath(new URL('./src/locales/**', import.meta.url)), dropMessageCompiler: true }),
    quasar(), // Required for tree-shaking... *sigh*
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '~mdi': fileURLToPath(new URL('./node_modules/@material-design-icons/svg/round', import.meta.url)),
    },
  },
  server: {
    proxy: {
      '/fr': {
        target: FLOP_BACKEND_URL,
        changeOrigin: true,
      },
      '/en': {
        target: FLOP_BACKEND_URL,
        changeOrigin: true,
      },
      '/es': {
        target: FLOP_BACKEND_URL,
        changeOrigin: true,
      },
      '/static': {
        target: FLOP_BACKEND_URL,
        changeOrigin: true,
      },
      '/api': {
        target: FLOP_BACKEND_URL,
        changeOrigin: true,
      },
    },
  },
  define: {
    'import.meta.vitest': 'undefined',
  },
  test: {
    // In-source testing
    include: ['src/**/*.{test,spec}.?(c|m)[jt]s?(x)', 'tests/**/*.{test,spec}.?(c|m)[jt]s?(x)'],
    includeSource: ['src/**/*.{js,ts}'],
    environment: 'happy-dom',
    reporters: ['default', 'html', 'junit'],
    outputFile: {
      html: './.reports/vitest/report.html',
      junit: './.reports/vitest/junit.xml',
    },
    coverage: {
      provider: 'v8',
      reporter: ['html', 'cobertura', 'text'],
      reportsDirectory: './.reports/vitest/coverage',
    },
  },
})
