import type { FilteredCourseColumns } from '@/components/management/courses/CoursesWrapper.vue'
import { Course } from '@/ts/type'
import { storeToRefs } from 'pinia'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'

const { selectedCourses, isModifying } = storeToRefs(useSelectedCourseStore())

/**
 * This function is used to select a range of courses from a block of FilteredCourses. It takes an array of FilteredCourses as a parameter.
 * If the `isModifying` state variable is false, it extracts all courses from the provided array of FilteredCourses using the `getCoursesFromFilteredCourses` function and adds them to the selectedCourses array.
 * It then removes any duplicate courses from the selectedCourses array.
 * If more than one course is added to the selectedCourses array, it sets `isMultiSelect` to true.
 *
 * @param {FilteredCourseColumns} filteredCourses - The array of FilteredCourses to select courses from.
 */
export function shiftCourseBlock(filteredCourses: FilteredCourseColumns) {
  if (!isModifying.value) {
    const courses: Course[] = getCoursesFromFilteredCourses(filteredCourses)
    selectedCourses.value = selectedCourses.value.concat(courses)
    // Remove duplicates
    selectedCourses.value = selectedCourses.value.filter((c, idx, arr) => arr.findIndex((el) => c.id === el.id) === idx)
  }
}

/**
 * This function is used to extract all courses from an array of FilteredCourses.
 * It takes an array of FilteredCourses as a parameter.
 * It iterates over each FilteredCourse in the array. If the FilteredCourse is a leaf node, it adds all its children (which are courses) to the courses array.
 * If the FilteredCourse is not a leaf node, it recursively calls itself with the children of the FilteredCourse (which are themselves FilteredCourses), and adds the returned courses to the courses array.
 * It returns the courses array, which contains all courses extracted from the provided array of FilteredCourses.
 *
 * @param {FilteredCourseColumns} filteredCourses - The array of FilteredCourses to extract courses from.
 * @returns {Course[]} - The array of courses extracted from the provided array of FilteredCourses.
 */
function getCoursesFromFilteredCourses(filteredCourses: FilteredCourseColumns): Course[] {
  // eslint being not very clever here. does not recognize type from vue files

  if (filteredCourses.leaf) {
    return filteredCourses.children
  }

  let courses: Course[] = []

  for (const filteredCourse of filteredCourses.left) {
    const childCourses = getCoursesFromFilteredCourses(filteredCourse.children)
    if (childCourses.length) courses = courses.concat(childCourses)
  }

  return courses
}

/**
 * This function is used to deselect all courses from the selectedCourses array.
 * If the `isModifying` state variable is false, it sets the selectedCourses array to an empty array and sets `isMultiSelect` to false.
 */
export function deselectCourses() {
  if (!isModifying.value) {
    selectedCourses.value = []
  }
}

/**
 * This function is used to select a single course. It takes a course and an event as parameters.
 * If the `isModifying` state variable is false, it stops the propagation of the event and sets the selectedCourses array to an array containing the provided course.
 * It also sets `isMultiSelect` to false, indicating that only one course is selected.
 *
 * @param {Course} course - The course to be selected.
 * @param {MouseEvent} event - The event that triggered the function.
 */
export function emitCourse(course: Course, event: MouseEvent) {
  if (!isModifying.value) {
    event.stopPropagation()
    selectedCourses.value = [course]
  }
}

/**
 * This function is used to add a course to the selectedCourses array. It takes a course as a parameter.
 * If the `isModifying` state variable is false, it checks if the selectedCourses array is empty.
 * If the selectedCourses array is empty, it sets the selectedCourses array to an array containing the provided course.
 * If the selectedCourses array is not empty, it checks if the provided course is already included in the selectedCourses array.
 * If the provided course is not included in the selectedCourses array, it adds the provided course to the selectedCourses array and sets `isMultiSelect` to true.
 * If the provided course is included in the selectedCourses array, it removes the provided course from the selectedCourses array and sets `isMultiSelect` to true if the selectedCourses array has more than one course after this operation.
 *
 * @param {Course} course - The course to be added to the selectedCourses array.
 */
export function addCourse(course: Course) {
  if (!isModifying.value) {
    if (selectedCourses.value.length === 0) {
      selectedCourses.value = [course]
    } else {
      if (!selectedCourses.value.map((course) => course.id).includes(course.id)) {
        selectedCourses.value.push(course)
      } else {
        selectedCourses.value = selectedCourses.value.filter((c) => c.id !== course.id)
      }
    }
  }
}

/**
 * This function is used to select a range of courses in a list. It takes a course and a list of courses as parameters.
 * If the last course in the selectedCourses array is not undefined and is present in the provided list of courses, it adds the provided course to the selectedCourses array.
 * It then finds the indices of the last course in the selectedCourses array and the provided course in the provided list of courses.
 * It iterates over the range of indices in the provided list of courses and adds each course in the range to the selectedCourses array if it's not already included.
 * If the selectedCourses array has more than one course after this operation, it sets isMultiSelect to true.
 * If the last course in the selectedCourses array is undefined or not present in the provided list of courses, it simply adds the provided course to the selectedCourses array.
 *
 * @param {Course} course - The course to be added to the selectedCourses array.
 * @param {Course[]} propsCourses - The list of courses to select a range from.
 */
export function shiftCourse(course: Course, propsCourses: Course[]) {
  if (!isModifying.value) {
    if (selectedCourses.value[selectedCourses.value.length - 1] !== undefined) {
      if (!propsCourses.some((c: Course) => c.id === selectedCourses.value[selectedCourses.value.length - 1]?.id)) {
        addCourse(course)
      }
      const start = propsCourses.indexOf(selectedCourses.value[selectedCourses.value.length - 1])
      const end = propsCourses.indexOf(course)

      if (start !== -1 && end !== -1) {
        for (let i = Math.min(start, end); i <= Math.max(start, end); i++) {
          if (!selectedCourses.value.find((course) => course.id === propsCourses[i].id)) {
            selectedCourses.value.push(propsCourses[i])
          }
        }
      }
    } else {
      addCourse(course)
    }
  }
}
