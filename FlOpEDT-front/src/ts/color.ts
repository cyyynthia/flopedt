import { getCourseType } from '@/stores/timetable/courseType'

const SATURATION: number = 75
const LIGHTNESS: number = 65

/**
 * Generates a list of colors based on the number of course types.
 * Each color is assigned to a course type and set as a CSS variable on the document root.
 */
export function generateColors() {
  const courseTypes = getCourseType()
  const step = 360 / courseTypes.length

  for (let i = 0; i < courseTypes.length; i++) {
    const hue = i * step

    // Set a CSS variable on the root element of the document.
    // The variable's name is `--color-` followed by the current course type.
    // The variable's value is the color corresponding to the current course type.
    document.documentElement.style.setProperty(
      `--color-${courseTypes[i]}`,
      `hsl(${hue}, ${SATURATION}%, ${LIGHTNESS}%)`
    )
  }
}
