<script setup lang="ts">
import type { Course } from '@/ts/type'

import { onMounted, ref } from 'vue'
import { storeToRefs } from 'pinia'
import { useAuth } from '@/stores/auth'
import { useDepartmentStore } from '@/stores/department'
import { useCourseStore } from '@/stores/timetable/course'
import { useRoomStore } from '@/stores/timetable/room'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useGroupStore } from '@/stores/timetable/group'
import { useCourseModuleStore } from '@/stores/timetable/courseModule'
import { SelectedFilter, useFilteredCourses } from '@/composables/filterCourses'
import { useCourseTypeStore } from '@/stores/timetable/courseType'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'
import { usePeriodStore } from '@/stores/timetable/period'

import FilterSelector from '@/components/management/filters/FilterSelector.vue'
import CoursesWrapper from '@/components/management/courses/CoursesWrapper.vue'
import CourseInformation from '@/components/management/detailed/CourseInformation.vue'
import CourseModification from '@/components/forms/CourseModification.vue'
import SingleCourseCreationDialog from '@/components/forms/courseCreation/SingleCourseCreationDialog.vue'
import MultipleCourseCreationDialog from '@/components/forms/courseCreation/MultipleCourseCreationDialog.vue'
import AlertDialog from '@/components/uikit/dialog/base/AlertDialog.vue'
import Dialog from '@/components/uikit/dialog/base/Dialog.vue'
import DialogClose from '@/components/uikit/dialog/DialogClose.vue'
import Toast from '@/components/uikit/Toast.vue'

const auth = useAuth()
const deptStore = useDepartmentStore()

const courseStore = useCourseStore()
const { isLoading, courses } = storeToRefs(courseStore)
const selectedCourseStore = useSelectedCourseStore()

const filters = ref<SelectedFilter[]>([])
const { filtered, entities } = useFilteredCourses(courses, auth.getUser.id, filters)

const { selectedCourses, isModifying } = storeToRefs(selectedCourseStore)

const enum DeletionState {
  IDLE,
  CONFIRM,
  IN_PROGRESS,
  ERRORED,
  SUCCESS,
}

const courseDeletionState = ref(DeletionState.IDLE)
const courseDeletionErrors = ref<Error[]>([])

async function deleteCourses() {
  courseDeletionState.value = DeletionState.IN_PROGRESS

  const errors: Error[] = []
  const stillSelected: Course[] = []
  for (const course of selectedCourses.value) {
    try {
      await courseStore.deleteCourse(course)
    } catch (e) {
      stillSelected.push(course)
      if (e instanceof Error) errors.push(e)
    }
  }

  if (!errors.length) {
    courseDeletionState.value = DeletionState.SUCCESS
    selectedCourses.value = []
    return
  }

  courseDeletionState.value = DeletionState.ERRORED
  courseDeletionErrors.value = errors
  selectedCourses.value = stillSelected
}

onMounted(() => {
  const courseTypeStore = useCourseTypeStore()
  const roomStore = useRoomStore()
  const moduleStore = useCourseModuleStore()
  const tutorStore = useTutorStore()
  const groupStore = useGroupStore()
  const periodStore = usePeriodStore()

  // Fetch courses
  deptStore.getDepartmentFromURL()
  void courseStore.fetchAllCourses(deptStore.current.abbrev)
  void courseTypeStore.fetchAllCourseTypes(deptStore.current.abbrev)
  void roomStore.fetchRooms(deptStore.current)
  void moduleStore.fetchAllCourseModules(deptStore.current.abbrev)
  void tutorStore.fetchTutors()
  void groupStore.fetchGroups(deptStore.current)
  void periodStore.fetchPeriods()
})
</script>

<template>
  <div v-if="isLoading" class="loading">
    {{ $t('common.loading') }}
  </div>
  <div v-else class="container">
    <div class="filters-column">
      <FilterSelector
        v-model="filters"
        :available-modules="entities.modules"
        :available-groups="entities.groups"
        :available-teachers="entities.teachers"
        :available-periods="entities.periods"
        :available-room-types="entities.roomTypes"
        :available-course-types="entities.courseTypes"
      />
    </div>
    <div class="courses-container">
      <div class="options-bar">
        <SingleCourseCreationDialog />
        <MultipleCourseCreationDialog />
      </div>
      <CoursesWrapper :courses="filtered" :filters="filters" />
    </div>
    <div v-if="selectedCourses.length > 0" class="visualization-column">
      <CourseModification v-if="isModifying" />
      <CourseInformation v-else @delete-courses="courseDeletionState = DeletionState.CONFIRM" />
    </div>
  </div>

  <AlertDialog
    :open="courseDeletionState === DeletionState.CONFIRM || courseDeletionState === DeletionState.IN_PROGRESS"
    :loading="courseDeletionState === DeletionState.IN_PROGRESS"
    @update:open="!$event && (courseDeletionState = DeletionState.IDLE)"
    @action="deleteCourses()"
  >
    <template #title>
      {{ $t('common.areYouSure') }}
    </template>
    <template #description>
      <p>{{ $t('management.courses.suppression.confirmation.header', courses.length) }}</p>
      <ul>
        <li v-for="course in selectedCourses" :key="course.id">
          {{
            $t('management.courses.suppression.confirmation.summary', {
              type: course.type.name,
              duration: course.duration,
              module: course.module.abbrev,
              tutor: course.tutor?.username ?? $t('management.courses.undefined'),
              period: course.period?.name ?? $t('management.courses.undefined'),
              groups: course.groups.map((g) => g.name).join(', '),
            })
          }}
        </li>
      </ul>
      <p>{{ $t('management.courses.suppression.confirmation.footer') }}</p>
    </template>
    <template #cancel>
      {{ $t('form.cancel') }}
    </template>
    <template #action>
      {{ $t('common.delete') }}
    </template>
  </AlertDialog>
  <Dialog
    :open="courseDeletionState === DeletionState.ERRORED"
    @update:open="!$event && (courseDeletionState = DeletionState.IDLE)"
  >
    <template #title>Une erreur est survenue</template>
    <template #description>
      <ul>
        <li v-for="error in courseDeletionErrors" :key="error.message">
          {{ error.message }}
        </li>
      </ul>
    </template>
    <template #footer>
      <DialogClose>Fermer</DialogClose>
    </template>
  </Dialog>
  <Toast
    :duration="5000"
    :open="courseDeletionState === DeletionState.SUCCESS"
    @update:open="!$event && (courseDeletionState = DeletionState.IDLE)"
  >
    <template #title>{{ $t('management.courses.suppression.success.title') }}</template>
    <template #description>{{ $t('management.courses.suppression.success.description') }}</template>
  </Toast>
</template>

<style scoped>
.loading,
.container {
  /* 124px is the height of the top navbar. Make sure to update if if required! */
  height: calc(100vh - 124px);
}

.loading {
  display: flex;
  align-items: center;
  justify-content: center;
}

.container {
  display: flex;
}

.filters-column,
.visualization-column {
  flex-shrink: 0;
  width: 300px;
  padding: 1rem;
  overflow-y: auto;
}

.filters-column {
  border-right: 1px currentColor solid;
}

.visualization-column {
  border-left: 1px currentColor solid;
}

.courses-container {
  display: flex;
  flex-direction: column;
  flex: 1;

  /* flexbox wizardry - prevent some unfortunate properties of flexbox to show up uninvited */
  min-width: 0;
}

.options-bar {
  display: flex;
  gap: 0.5rem;
  padding: 0.5rem;
  border-bottom: 1px currentColor solid;
}
</style>
