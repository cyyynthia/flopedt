<script setup lang="ts">
import type { CourseUpdateData } from '@/ts/type'

import { computed, ref, watch } from 'vue'
import { storeToRefs } from 'pinia'
import { useI18n } from 'vue-i18n'

import { useCourseModuleStore } from '@/stores/timetable/courseModule'
import { useCourseTypeStore } from '@/stores/timetable/courseType'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useGroupStore } from '@/stores/timetable/group'
import { usePeriodStore } from '@/stores/timetable/period'
import { useCourseStore } from '@/stores/timetable/course'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'

import Button, { Color, Look } from '@/components/uikit/Button.vue'
import SelectInput from '@/components/uikit/form/input/select/SelectInput.vue'
import SelectItem from '@/components/uikit/form/input/select/SelectItem.vue'
import ComboboxInput from '@/components/uikit/form/input/combobox/ComboboxInput.vue'
import ComboboxItem from '@/components/uikit/form/input/combobox/ComboboxItem.vue'
import TagsInputCombobox from '@/components/uikit/form/input/tagsinput/TagsInputCombobox.vue'
import MultipleValueGatedField from '@/components/forms/MultipleValueGatedField.vue'
import Loader from '@/components/uikit/Loader.vue'

const NULL_ID = -1
const DURATION_RE = /^(((\d+:)?[0-5]\d)|\d):[0-5]\d$/

const ERRORS_CLEAR = {
  message: null as string | null,
  groups: null as string | null,
  duration: null as string | null,
}

const enum FormState {
  IDLE,
  SUBMITTING,
  ERRORED,
}

const { t } = useI18n()
const selectedCourseStore = useSelectedCourseStore()
const { selectedCourses, isModifying } = storeToRefs(selectedCourseStore)

const courseStore = useCourseStore()
const courseModulesStore = useCourseModuleStore()
const tutorStore = useTutorStore()
const courseTypeStore = useCourseTypeStore()
const groupStore = useGroupStore()
const periodStore = usePeriodStore()

const formState = ref(FormState.IDLE)
const errors = ref(ERRORS_CLEAR)

const groupOptions = computed(() => groupStore.groups.map((t) => ({ value: t.id, name: t.name })))
const tutorOptions = computed(() =>
  tutorStore.tutors.map((t) => ({
    value: t.id,
    name: `${t.firstname} ${t.lastname}`,
    alias: [t.username],
  }))
)

const tutorOptionsMain = computed(() =>
  tutorOptions.value.map((t) => ({ ...t, disabled: suppTutors.value.includes(t.value) }))
)
const tutorOptionsSupp = computed(() => tutorOptions.value.map((t) => ({ ...t, disabled: t.value === tutor.value })))

const courseTypeEditOverride = ref(selectedCourseStore.unique.type)
const moduleEditOverride = ref(selectedCourseStore.unique.module)
const tutorEditOverride = ref(selectedCourseStore.unique.tutor)
const suppTutorsEditOverride = ref(selectedCourseStore.unique.suppTutors)
const groupsEditOverride = ref(selectedCourseStore.unique.groups)
const periodEditOverride = ref(selectedCourseStore.unique.period)
const durationEditOverride = ref(selectedCourseStore.unique.duration)

const baseCourse = selectedCourses.value[0]
const courseType = ref(baseCourse.type.id.toString())
const module = ref(baseCourse.module.id)
const tutor = ref(baseCourse.tutor?.id ?? NULL_ID)
const suppTutors = ref(baseCourse.supp_tutors.map((st) => st.id))
const groups = ref(baseCourse.groups.map((g) => g.id))
const period = ref(baseCourse.period?.id ?? NULL_ID)
const duration = ref(baseCourse.duration)

watch(groups, () => (errors.value.groups = null))
watch(duration, () => (errors.value.duration = null))

// This will be more friendly to deal with once UIKit is more complete and Field elements are there
// <editor-fold name="Combobox filters and display functions">
const createFilterByName = (entities: Array<{ id: number; name: string }>) =>
  function (_values: unknown[], term: string): string[] {
    term = term.toLowerCase()
    return entities
      .filter((e) => e.name.toLowerCase().includes(term))
      .map((e) => e.id as unknown as string /* Make Radix happy... */)
  }

const filterModule = computed(() => createFilterByName(courseModulesStore.courseModules))
const filterPeriod = computed(() => createFilterByName(periodStore.periods))
function filterTutor(_values: unknown[], term: string): string[] {
  term = term.toLowerCase()
  return tutorStore.tutors
    .filter(
      (t) =>
        t.lastname.toLowerCase().includes(term) ||
        t.firstname.toLowerCase().includes(term) ||
        t.username.toLowerCase().includes(term)
    )
    .map((e) => e.id as unknown as string /* Make Radix happy... */)
}

const displayModule = (id: number) =>
  id === NULL_ID ? t('form.noSelection') : courseModulesStore.courseModules.find((m) => m.id === id)?.name
const displayPeriod = (id: number) =>
  id === NULL_ID ? t('form.noSelection') : periodStore.periods.find((p) => p.id === id)?.name
function displayTutor(id: number) {
  if (id === NULL_ID) return t('form.noSelection')
  const tutor = tutorStore.tutors.find((t) => t.id === id)
  return tutor ? `${tutor.firstname} ${tutor.lastname}` : '???'
}
// </editor-fold>

async function submitForm() {
  formState.value = FormState.SUBMITTING
  errors.value = ERRORS_CLEAR

  if (groups.value.length === 0) {
    formState.value = FormState.ERRORED
    errors.value.groups = 'required'
  }

  if (!DURATION_RE.test(duration.value)) {
    formState.value = FormState.ERRORED
    errors.value.duration = duration.value ? 'format' : 'required'
  }

  // Halt if we have errors
  if (formState.value === FormState.ERRORED) return

  formState.value = FormState.SUBMITTING
  const update: Omit<CourseUpdateData, 'id'> = {}

  if (courseTypeEditOverride.value) update.type_id = Number(courseType.value)
  if (moduleEditOverride.value) update.module_id = module.value
  if (tutorEditOverride.value) update.tutor_id = tutor.value === NULL_ID ? void 0 : tutor.value
  if (suppTutorsEditOverride.value) update.supp_tutors = suppTutors.value.map((id) => ({ id }))
  if (groupsEditOverride.value) update.groups = groups.value.map((id) => ({ id }))
  if (periodEditOverride.value) update.period_id = period.value === NULL_ID ? void 0 : period.value
  if (durationEditOverride.value) update.duration = duration.value

  const pushErrors: [number, Error][] = []
  for (const course of selectedCourses.value) {
    try {
      // TODO(Cynthia): That's kinda annoying... but the store may be ported as a "generic" crud store anyway
      await courseStore.updateCourse({ id: course.id, ...update })
    } catch (e) {
      pushErrors.push([course.id, e as Error])
    }
  }

  if (pushErrors.length) {
    formState.value = FormState.ERRORED

    // TODO: i18n, more robust reporting, etc
    errors.value.message = pushErrors.length === 1 ? 'An error occurred' : `${pushErrors.length} errors occurred`
    errors.value.message += ' while updating the courses.\n'
    for (const [courseId, err] of pushErrors) {
      errors.value.message += `\n- Course #${courseId}: ${err.message}`
    }

    return
  }

  isModifying.value = false
}
</script>

<template>
  <h3>{{ $t('management.courses.update', selectedCourses.length) }}</h3>
  <div v-if="errors.message" class="error-message">
    {{ errors.message }}
  </div>
  <form class="form" @submit.prevent="submitForm">
    <div class="field">
      <b>{{ $t('management.courses.courseType.simple') }}</b>
      <MultipleValueGatedField v-model="courseTypeEditOverride" :unique="selectedCourseStore.unique.type">
        <SelectInput v-model="courseType" :placeholder="$t('management.courses.courseType.placeholder')">
          <SelectItem v-for="type in courseTypeStore.courseTypes" :key="type.id" :value="type.id.toString()">
            {{ type.name }}
          </SelectItem>
        </SelectInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.module.simple') }}</b>
      <MultipleValueGatedField v-model="moduleEditOverride" :unique="selectedCourseStore.unique.module">
        <ComboboxInput
          v-model="module"
          :placeholder="$t('management.courses.module.placeholder')"
          :filter-function="filterModule"
          :display-value="displayModule"
        >
          <ComboboxItem v-for="mdl in courseModulesStore.courseModules" :key="mdl.id" :value="mdl.id">
            {{ mdl.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.tutor.simple') }}</b>
      <MultipleValueGatedField v-model="tutorEditOverride" :unique="selectedCourseStore.unique.tutor">
        <ComboboxInput
          v-model="tutor"
          :placeholder="$t('management.courses.tutor.placeholder')"
          :filter-function="filterTutor"
          :display-value="displayTutor"
        >
          <ComboboxItem :value="NULL_ID">
            <i>{{ $t('form.noSelection') }}</i>
          </ComboboxItem>
          <ComboboxItem v-for="tut in tutorOptionsMain" :key="tut.value" :value="tut.value" :disabled="tut.disabled">
            {{ tut.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.suppTutors.simple', Number.MAX_SAFE_INTEGER) }}</b>
      <MultipleValueGatedField v-model="suppTutorsEditOverride" :unique="selectedCourseStore.unique.suppTutors">
        <TagsInputCombobox
          v-model="suppTutors"
          :placeholder="$t('management.courses.suppTutors.placeholder')"
          :options="tutorOptionsSupp"
        />
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.group.simple', Number.MAX_SAFE_INTEGER) }}</b>
      <MultipleValueGatedField v-model="groupsEditOverride" :unique="selectedCourseStore.unique.groups">
        <TagsInputCombobox
          v-model="groups"
          :placeholder="$t('management.courses.group.placeholder')"
          :options="groupOptions"
        />
      </MultipleValueGatedField>
      <div v-if="errors.groups" class="form-error">{{ $t(`form.errors.${errors.groups}`) }}</div>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.period.simple') }}</b>
      <MultipleValueGatedField v-model="periodEditOverride" :unique="selectedCourseStore.unique.period">
        <ComboboxInput
          v-model="period"
          :placeholder="$t('management.courses.period.placeholder')"
          :filter-function="filterPeriod"
          :display-value="displayPeriod"
        >
          <ComboboxItem :value="NULL_ID">
            <i>{{ $t('form.noSelection') }}</i>
          </ComboboxItem>
          <ComboboxItem v-for="per in periodStore.periods" :key="per.id" :value="per.id">
            {{ per.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.duration.simple') }}</b>
      <MultipleValueGatedField v-model="durationEditOverride" :unique="selectedCourseStore.unique.duration">
        <!-- TODO: make UIKit element -->
        <input v-model="duration" type="text" class="flopui-input" placeholder="00:00" />
        <div v-if="errors.duration" class="form-error">{{ $t(`form.errors.${errors.duration}`) }}</div>
      </MultipleValueGatedField>
    </div>

    <div class="buttons-container">
      <Button :color="Color.GREEN" :disabled="formState === FormState.SUBMITTING">
        <template v-if="formState === FormState.SUBMITTING">
          <Loader />
          <span>{{ $t('form.saving') }}</span>
        </template>
        <template v-else>{{ $t('form.save') }}</template>
      </Button>
      <Button
        :look="Look.OUTLINED"
        :color="Color.RED"
        :disabled="formState === FormState.SUBMITTING"
        @click="isModifying = false"
      >
        {{ $t('form.cancel') }}
      </Button>
    </div>
  </form>
</template>

<style scoped>
h3 {
  font-size: 1.5rem;
  margin: 0 0 1.5rem;
}

.error-message {
  /* TODO: UIKit component for all "boxed" messages */
  background-color: rgba(var(--color-red-rgb), 0.2);
  padding: 0.5rem;
  border-radius: 4px;
  margin-bottom: 1rem;
  white-space: pre-wrap;
}

.form-error {
  /* This will no longer be needed once we have proper input fields */
  font-size: 0.875rem;
  color: var(--color-red);
  margin-top: 0.25rem;
}

.form {
  display: flex;
  flex-direction: column;
  list-style-type: none;
  padding-left: 0;
  gap: 1.25rem;
}

.field > b {
  display: block;
  margin-bottom: 0.25rem;
}

.buttons-container {
  display: flex;
  justify-content: flex-end;
  width: 100%;
  gap: 0.5rem;
}
</style>
