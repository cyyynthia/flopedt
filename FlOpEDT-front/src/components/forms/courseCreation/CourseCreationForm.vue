<script lang="ts">
export enum CourseFormState {
  SINGLE = 'single',
  GLOBAL = 'global',
  PROPAGATED = 'propagated',
}

export interface AttributeEntity {
  /** ID of the selected entity */
  value: number
  /** Name of the entity */
  name: string
  /** Name of the entity's alias */
  alias?: string[]
}

export interface CourseFormData {
  courseTypes: number[]
  modules: number[]
  tutorAuto: boolean
  tutors: number[]
  suppTutors: number[]
  groupsPreFilled: boolean
  groupSubSelection: boolean
  groups: number[]
  periods: number[]
  courseDurationHours: number
  courseDurationMinutes: number
  roomType: number
  comment: string
  coursesNumber: number
}

export interface CreationErrors {
  courseTypes: string | null
  modules: string | null
  tutors: string | null
  groups: string | null
  courseDuration: string | null
  coursesNumber: string | null
}
</script>

<script setup lang="ts">
import ComboboxItem from '@/components/uikit/form/input/combobox/ComboboxItem.vue'
import ComboboxInput from '@/components/uikit/form/input/combobox/ComboboxInput.vue'
import TagsInputCombobox from '@/components/uikit/form/input/tagsinput/TagsInputCombobox.vue'
import { useI18n } from 'vue-i18n'
import { computed } from 'vue'

const { t } = useI18n()

const NULL_ID = -1

interface Props {
  datas: {
    modules: AttributeEntity[]
    groups: AttributeEntity[]
    tutors: AttributeEntity[]
    periods: AttributeEntity[]
    roomTypes: AttributeEntity[]
    courseTypes: AttributeEntity[]
  }
  formState: CourseFormState
  errors?: CreationErrors
}

const props = defineProps<Props>()
const formData = defineModel<CourseFormData>({ required: true })

const locks = {
  courseTypesLock: false,
  modulesLock: false,
  tutorsLock: false,
  suppTutorsLock: false,
  groupsLock: false,
  periodsLock: false,
}

const createFilterByName = (entities: Array<{ value: number; name: string }>) =>
  function (_values: unknown[], term: string): string[] {
    term = term.toLowerCase()
    return entities
      .filter((e) => e.name.toLowerCase().includes(term))
      .map((e) => e.value as unknown as string /* Make Radix happy... */)
  }

const filterCourseType = computed(() => createFilterByName(props.datas.courseTypes))
const filterModule = computed(() => createFilterByName(props.datas.modules))
function filterTutor(_values: unknown[], term: string): string[] {
  term = term.toLowerCase()
  return props.datas.tutors
    .filter((t) => t.alias?.some((alias) => alias.toLowerCase().includes(term)) || t.name.toLowerCase().includes(term))
    .map((e) => e.value as unknown as string /* Make Radix happy... */)
}
const filterPeriod = computed(() => createFilterByName(props.datas.periods))
const filterRoomType = computed(() => createFilterByName(props.datas.roomTypes))

const displayCourseTypes = (id: number) =>
  id == NULL_ID ? t('form.noSelection') : props.datas.courseTypes.find((c) => c.value === id)?.name
const displayModule = (id: number) =>
  id == NULL_ID ? t('form.noSelection') : props.datas.modules.find((m) => m.value === id)?.name
const displayTutor = (id: number) =>
  id == NULL_ID ? t('form.noSelection') : props.datas.tutors.find((t) => t.value === id)?.name
const displayPeriod = (id: number) =>
  id == NULL_ID ? t('form.noSelection') : props.datas.periods.find((r) => r.value === id)?.name
const displayRoomType = (id: number) =>
  id == NULL_ID ? t('form.noSelection') : props.datas.roomTypes.find((p) => p.value === id)?.name

// Modifies formData elements depending on state and needs
if (props.formState !== CourseFormState.PROPAGATED) {
  // Initializes non tags input - input fields as NULL_ID in order to select the none option
  if (formData.value.courseTypes.length === 0) formData.value.courseTypes = [NULL_ID]
  if (formData.value.modules.length === 0) formData.value.modules = [NULL_ID]
  if (formData.value.tutors.length === 0) formData.value.tutors = [NULL_ID]
  if (formData.value.periods.length === 0) formData.value.periods = [NULL_ID]
} else {
  // Erases NULL_ID values in datas
  if (formData.value.courseTypes[0] === NULL_ID) formData.value.courseTypes = []
  if (formData.value.modules[0] === NULL_ID) formData.value.modules = []
  if (formData.value.tutors[0] === NULL_ID) formData.value.tutors = []
  if (formData.value.periods[0] === NULL_ID) formData.value.periods = []

  // Set up locks based on pre-filled content
  locks.courseTypesLock = formData.value.courseTypes.length >= 1
  locks.modulesLock = formData.value.modules.length >= 1
  locks.tutorsLock = formData.value.tutors.length >= 1
  locks.suppTutorsLock = formData.value.suppTutors.length >= 1
  locks.groupsLock = formData.value.groups.length >= 1
  locks.periodsLock = formData.value.periods.length >= 1
}
</script>

<template>
  <div class="form">
    <!-- Course Type Section -->
    <div class="form-section">
      <p class="form-section-title">{{ $t('management.courses.courseType.simple') }} *</p>
      <div class="form-section-content">
        <div v-if="errors?.courseTypes" class="form-error">{{ $t(`form.errors.${errors.courseTypes}`) }}</div>
        <ComboboxInput
          v-if="formState != CourseFormState.PROPAGATED"
          v-model="formData.courseTypes[0]"
          :display-value="displayCourseTypes"
          :filter-function="filterCourseType"
        >
          <ComboboxItem :value="NULL_ID"> {{ $t('form.noSelection') }} </ComboboxItem>
          <ComboboxItem v-for="courseType in datas.courseTypes" :key="courseType.value" :value="courseType.value">
            {{ courseType.name }}
          </ComboboxItem>
        </ComboboxInput>
        <div v-else class="multi-input">
          <div v-if="locks.courseTypesLock" class="locked-option">
            {{ datas.courseTypes.find((ct) => ct.value === formData.courseTypes[0])?.name }}
          </div>
          <TagsInputCombobox
            v-else
            v-model="formData.courseTypes"
            :options="datas.courseTypes"
            :placeholder="$t('management.courses.courseType.placeholder')"
          />
        </div>
      </div>
    </div>

    <!-- Modules Section -->
    <div class="form-section">
      <p class="form-section-title">{{ $t('management.courses.module.simple') }} *</p>
      <div class="form-section-content">
        <div v-if="errors?.modules" class="form-error">{{ $t(`form.errors.${errors.modules}`) }}</div>
        <ComboboxInput
          v-if="formState != CourseFormState.PROPAGATED"
          v-model="formData.modules[0]"
          :display-value="displayModule"
          :filter-function="filterModule"
        >
          <ComboboxItem :value="NULL_ID">{{ $t('form.noSelection') }}</ComboboxItem>
          <ComboboxItem v-for="module in datas.modules" :key="module.value" :value="module.value">
            {{ module.name }}
          </ComboboxItem>
        </ComboboxInput>
        <div v-else class="multi-input">
          <div v-if="locks.modulesLock" class="locked-option">
            {{ datas.modules.find((m) => m.value === formData.modules[0])?.name }}
          </div>
          <TagsInputCombobox
            v-else
            v-model="formData.modules"
            :options="datas.modules"
            :placeholder="$t('management.courses.module.placeholder')"
          />
        </div>
      </div>
    </div>

    <!-- Tutors Section -->
    <div class="form-large-section">
      <div class="form-section">
        <p class="form-section-title">{{ $t('management.courses.tutor.plural') }}</p>
        <div
          v-if="formState == CourseFormState.PROPAGATED && !(locks.tutorsLock && locks.suppTutorsLock)"
          class="form-section-content"
        >
          <div class="radio-group">
            <div class="radio-btn">
              <label>
                <i>{{ $t('form.inputMode.manual') }}</i>
                <input type="radio" name="teacher-mode" value="manual-teacher" :checked="true" />
              </label>
            </div>
            <div class="radio-btn">
              <label>
                <i>{{ $t('form.inputMode.auto') }}</i>
                <input type="radio" name="teacher-mode" value="auto-teacher" />
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="form-section">
        <p class="form-section-sub-title">- {{ $t('management.courses.tutor.simple') }}</p>
        <div class="form-section-content">
          <ComboboxInput
            v-if="formState != CourseFormState.PROPAGATED"
            v-model="formData.tutors[0]"
            :display-value="displayTutor"
            :filter-function="filterTutor"
          >
            <ComboboxItem :value="NULL_ID"> {{ $t('form.noSelection') }} </ComboboxItem>
            <ComboboxItem v-for="tutor in datas.tutors" :key="tutor.value" :value="tutor.value">
              {{ tutor.name }}
            </ComboboxItem>
          </ComboboxInput>
          <div v-else class="multi-input">
            <div v-if="locks.tutorsLock" class="locked-option">
              {{ datas.tutors.find((st) => st.value === formData.tutors[0])?.name }}
            </div>
            <TagsInputCombobox
              v-else
              v-model="formData.tutors"
              :options="datas.tutors"
              :placeholder="$t('management.courses.tutor.placeholder')"
            />
          </div>
        </div>
      </div>
      <div class="form-section">
        <p class="form-section-sub-title">
          - {{ $t('management.courses.suppTutors.simple', Number.MAX_SAFE_INTEGER) }}
        </p>
        <div class="form-section-content">
          <div class="multi-input">
            <div v-if="errors?.tutors" class="form-error">{{ $t(`form.errors.${errors.tutors}`) }}</div>
            <div v-if="formState == CourseFormState.PROPAGATED && locks.suppTutorsLock" class="locked-option">
              {{
                formData.suppTutors.map((tutorId) => datas.tutors.find((st) => st.value == tutorId)?.name).join(', ')
              }}
            </div>
            <div v-else-if="formState == CourseFormState.PROPAGATED" class="locked-option">
              {{ $t('form.noSelection') }}
            </div>
            <TagsInputCombobox
              v-else
              v-model="formData.suppTutors"
              :options="datas.tutors"
              :placeholder="$t('management.courses.suppTutors.placeholder')"
            />
          </div>
        </div>
      </div>
    </div>

    <!-- Groups Section -->
    <div class="form-section">
      <p class="form-section-title">{{ $t('management.courses.group.simple', Number.MAX_SAFE_INTEGER) }} *</p>
      <div class="form-large-section-content">
        <div v-if="formState == CourseFormState.PROPAGATED && !locks.groupsLock" class="form-section-content">
          <div class="radio-group" style="display: flex">
            <div class="radio-btn" style="margin-right: 25px">
              <label>
                <i>{{ $t('form.groupOptions.each') }}</i>
                <input type="radio" name="group-mode" value="each-group" :checked="true" />
              </label>
            </div>
            <div class="radio-btn">
              <label>
                <i>{{ $t('form.groupOptions.sub') }}</i>
                <input type="radio" name="group-mode" value="sub-groups" />
              </label>
            </div>
          </div>
        </div>
        <div class="form-section-content">
          <div class="multi-input">
            <div v-if="errors?.groups" class="form-error">{{ $t(`form.errors.${errors.groups}`) }}</div>
            <div v-if="formState == CourseFormState.PROPAGATED && locks.groupsLock" class="locked-option">
              {{ formData.groups.map((groupId) => datas.groups.find((g) => g.value == groupId)?.name).join(', ') }}
            </div>
            <TagsInputCombobox
              v-else
              v-model="formData.groups"
              :options="datas.groups"
              :placeholder="$t('management.courses.group.placeholder')"
            />
          </div>
        </div>
      </div>
    </div>

    <!-- Period Section -->
    <div class="form-section">
      <p class="form-section-title">{{ $t('management.courses.period.simple') }}</p>
      <div class="form-section-content">
        <ComboboxInput
          v-if="formState != CourseFormState.PROPAGATED"
          v-model="formData.periods[0]"
          :display-value="displayPeriod"
          :filter-function="filterPeriod"
        >
          <ComboboxItem :value="NULL_ID"> {{ $t('form.noSelection') }} </ComboboxItem>
          <ComboboxItem v-for="period in datas.periods" :key="period.value" :value="period.value">
            {{ period.name }}
          </ComboboxItem>
        </ComboboxInput>
        <div v-else class="multi-input">
          <div v-if="locks.periodsLock" class="locked-option">
            {{ datas.periods.find((p) => p.value === formData.periods[0])?.name }}
          </div>
          <TagsInputCombobox
            v-else
            v-model="formData.periods"
            :options="datas.periods"
            :placeholder="$t('management.courses.period.placeholder')"
          />
        </div>
      </div>
    </div>

    <!-- Course Duration Section -->
    <div v-if="formState != CourseFormState.PROPAGATED" class="form-section">
      <p class="form-section-title">{{ $t('management.courses.duration.simple') }}</p>
      <div class="form-section-content">
        <div v-if="errors?.courseDuration" class="form-error">{{ $t(`form.errors.${errors.courseDuration}`) }}</div>
        <div class="time-selector">
          <input
            v-model="formData.courseDurationHours"
            class="num-stepper"
            type="number"
            name="nb-course"
            min="1"
            max="12"
            value="1"
          />
          <label>{{ $t('management.courses.duration.hours') }}</label>
          <input
            v-model="formData.courseDurationMinutes"
            class="num-stepper"
            type="number"
            name="nb-course"
            min="0"
            max="59"
            step="5"
            value="0"
          />
          <label>{{ $t('management.courses.duration.minutes') }}</label>
        </div>
      </div>
    </div>

    <!-- Room Type Section -->
    <div v-if="formState != CourseFormState.PROPAGATED" class="form-section">
      <p class="form-section-title">{{ $t('management.courses.roomType.simple') }} *</p>
      <div class="form-section-content">
        <ComboboxInput v-model="formData.roomType" :display-value="displayRoomType" :filter-function="filterRoomType">
          <ComboboxItem :value="NULL_ID">{{ $t('form.noSelection') }}</ComboboxItem>
          <ComboboxItem v-for="roomType in datas.roomTypes" :key="roomType.value" :value="roomType.value">
            {{ roomType.name }}
          </ComboboxItem>
        </ComboboxInput>
      </div>
    </div>

    <!-- Comment Section -->
    <div v-if="formState != CourseFormState.PROPAGATED" class="form-section">
      <p class="form-section-title">{{ $t('management.courses.comment.simple') }}</p>
      <div class="form-section-content">
        <textarea id="comment" v-model="formData.comment" name="comment" class="text-area" rows="5" cols="30" />
      </div>
    </div>

    <!-- Number Of Courses Section -->
    <div v-if="props.formState != CourseFormState.GLOBAL" class="form-section">
      <p class="form-section-title">{{ $t('form.coursesNumber') }}</p>
      <div class="form-section-content">
        <div v-if="errors?.coursesNumber" class="form-error">{{ $t(`form.errors.${errors.coursesNumber}`) }}</div>
        <input
          id="nb-courses"
          v-model="formData.coursesNumber"
          class="flopui-input"
          type="number"
          name="nb-course"
          min="1"
          max="100"
        />
      </div>
    </div>
  </div>
</template>

<style scoped>
p {
  margin: 0;
}

.form {
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
}

.form-section {
  display: flex;
}

.form-section-title,
.form-section-sub-title {
  font-weight: 600;
  line-height: 2rem;
  width: 220px;
}

.form-section-sub-title {
  padding-left: 10px;
  font-size: 0.875rem;
}

.form-section-content,
.form-large-section-content {
  font-size: 0.875rem;
  min-height: 2rem;
  flex: 1;
}

.form-large-section {
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
}

.form-error {
  /* This will no longer be needed once we have proper input fields */
  font-size: 0.875rem;
  color: var(--color-red);
  margin-top: 0.25rem;
}

.time-selector {
  display: flex;
  align-items: center;
  min-height: 2rem;
  gap: 0.5rem;
}

.num-stepper,
.text-area {
  border: 1px rgba(var(--text-color-rgb), 0.2) solid;
  border-radius: 0.25rem;
}

.num-stepper {
  width: 45px;
  height: 20px;
  padding: 0 0 0 5px;
}

.text-area {
  padding: 0.5rem;
  font-family: inherit;
  font-size: 0.875rem;
  width: 100%;
}

.radio-group {
  display: flex;
  align-items: center;
  gap: 2rem;
}

.radio-btn label {
  display: flex;
  align-items: center;
  height: 2rem;
  gap: 0.5rem;
}

.locked-option {
  display: flex;
  line-height: 2rem;
}
</style>
