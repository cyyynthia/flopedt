---
group: uikit
title: Icons
icon: carbon:document
---

# Using icons

To use icons, you can simply import them as if they were a Vue component, and use them as you would usually. This
is powered by [vite-plugin-magical-svg](https://github.com/cyyynthia/vite-plugin-magical-svg).

You can treat the component as a basic `<svg>`. All props will be forwarded to the element.

```vue{2,6}
<script setup lang="ts">
import MyIcon from '@/assets/icons/icon.svg'
</script>

<template>
  <MyIcon class="icon" />
</template>

<style scoped>
.icon {
  fill: currentColor;
}
</style>
```

It is possible to customize certain properties of the import methodology via query parameters on the import. Please
see the plugin's [documentation](https://github.com/cyyynthia/vite-plugin-magical-svg#use-in-code).

## Icon pack

Material design icons are available through the `@material-design-icons/svg` package. To make imports shorter, the
`~mdi` alias is available (points to `@material-design-icons/svg/round`).

You can explore the available icons [here](https://marella.me/material-design-icons/demo/svg/)

```vue{2}
<script setup lang="ts">
import Home from '~mdi/home.svg'
</script>

<template>
  <Home class="icon" />
</template>
```

## Inner workings

Under the hood, the plugin creates an SVG sprite-sheet composed of all the SVG that were imported. They are then
referenced using [`<use>`](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/use) which is way more lightweight
than having the entire rendered through VDOM.

It also supports using `currentColor` to change the color of the symbol, which makes it much more suitable for general
purpose usage compared to using `<img src="..." alt="..."/>`.
