import { Timestamp } from '@quasar/quasar-ui-qcalendar'

import Battery0BarIcon from '~mdi/battery_0_bar.svg'
import Battery1BarIcon from '~mdi/battery_1_bar.svg'
import Battery2BarIcon from '~mdi/battery_2_bar.svg'
import Battery3BarIcon from '~mdi/battery_3_bar.svg'
import Battery4BarIcon from '~mdi/battery_4_bar.svg'
import Battery5BarIcon from '~mdi/battery_5_bar.svg'
import Battery6BarIcon from '~mdi/battery_6_bar.svg'
import BatteryFullIcon from '~mdi/battery_full.svg'
import BatteryChargingIcon from '~mdi/battery_charging_full.svg'

/**
 * Calendar event, for display purpose
 */
export interface CalendarEventNoCol {
  id: number
  title: string

  toggled: boolean

  bgcolor: string
  icon?: string

  data: EventData
}

export interface InputCalendarEvent extends CalendarEventNoCol {
  columnIds: number[]
}

export interface CalendarEvent extends CalendarEventNoCol {
  spans: Array<{ istart: number; weight: number; columnIds: number[] }>
}

export interface EventData {
  dataId: number
  dataType: 'event' | 'dropzone' | 'header' | 'avail'
  start: Timestamp
  duration?: number
  value?: number
}

/**
 * Calendar column, to divide each day in several columns
 */
export interface CalendarColumn {
  id: number
  name: string
  weight: number
  /**
   * Position of the column in the abscissa
   */
  // x: number
  // active: boolean
}

export interface CalendarResourceScope {
  resource: { id: number }
  timeStartPosX(time: Timestamp): number
  timeDurationWidth(duration: number): number
}

// This is expected to change, but for now, let's start with that
export interface CalendarResourceEvent {
  left: number
  width: number
  title: string
}

interface AvailabilityData {
  color: Record<string, string>
  icon: Record<string, string>
}

export const availabilityData: AvailabilityData = {
  color: {
    '0': '#fc0328',
    '1': '#fc3403',
    '2': '#fc6703',
    '3': '#faa305',
    '4': '#faf405',
    '5': '#a4fa05',
    '6': '#53fd02',
    '7': '#04fb13',
    '8': '#00ff5e',
  },
  icon: {
    '0': Battery0BarIcon,
    '1': Battery1BarIcon,
    '2': Battery2BarIcon,
    '3': Battery3BarIcon,
    '4': Battery4BarIcon,
    '5': Battery5BarIcon,
    '6': Battery6BarIcon,
    '7': BatteryFullIcon,
    '8': BatteryChargingIcon,
  },
}
