<script setup lang="ts">
import { computed, nextTick, ref } from 'vue'
import Button, { Color, Look } from '@/components/uikit/Button.vue'
import ButtonGroup from '@/components/uikit/ButtonGroup.vue'
import TagsInputCombobox from '@/components/uikit/form/input/tagsinput/TagsInputCombobox.vue'
import FilterIcon from '@/components/management/filters/FilterIcon.vue'

import { FilterEntity, FilterType } from '@/composables/filterCourses.js'

import Remove from '~mdi/remove.svg'
import DragIndicator from '~mdi/drag_indicator.svg'
import Expand from '~mdi/expand_more.svg'
import MingcuteIntersectFill from '@/assets/icons/mingcute-intersect-fill.svg'
import MingcuteExcludeFill from '@/assets/icons/mingcute-exclude-fill.svg'

interface Props {
  filter: FilterType
  entities: FilterEntity[]
}

type Emit = {
  delete: []
  dragstart: [MouseEvent]
}

const props = defineProps<Props>()

const selected = defineModel<number[]>('selected', { required: true })
const exclude = defineModel<boolean>('exclude', { required: true })

const emit = defineEmits<Emit>()

const containerRef = ref<HTMLElement>()
const expandedWrapperRef = ref<HTMLElement>()

const expanded = ref(false)

const firstSelected = computed(() => {
  return props.entities.find((e) => e.id === selected.value[0])?.name
})

function handleToggleExpanded() {
  if (!expandedWrapperRef.value) {
    // Should never be happening, but handled gracefully anyway
    expanded.value = !expanded.value
    return
  }

  if (expanded.value) {
    // This allows to bypass the Vue render pipeline and immediately start animations
    containerRef.value?.classList?.remove('expanded')

    if (expandedWrapperRef.value) {
      // Necessary for the animation to work as expected
      const { height } = expandedWrapperRef.value.getBoundingClientRect()
      expandedWrapperRef.value.style.height = `${height}px` // Necessary for the animation to work as expected
    }

    // Start the close animation next tick (so height gets applied)
    window.requestAnimationFrame(() => {
      expandedWrapperRef.value?.removeAttribute('style')

      // Actually close the thing 250ms later, to let the animations run
      setTimeout(() => (expanded.value = false), 250)
    })

    return
  }

  expanded.value = true
  void nextTick(() => {
    if (!expandedWrapperRef.value) return

    const targetHeight = expandedWrapperRef.value.firstElementChild?.getBoundingClientRect().height
    if (targetHeight) {
      expandedWrapperRef.value.style.height = `${targetHeight}px`
      setTimeout(() => {
        if (!expandedWrapperRef.value) return

        // These are not necessary once the animation is complete and break the behavior of the element
        // So let's get rid of them while it's not being animated
        expandedWrapperRef.value.style.overflow = 'visible'
        expandedWrapperRef.value.style.height = 'auto'
      }, 250)
    }
  })
}

function handleDragStart(event: MouseEvent) {
  if (expanded.value && expandedWrapperRef.value) {
    expandedWrapperRef.value.style.transition = 'none'
    expandedWrapperRef.value.style.height = '0px'
    emit('dragstart', event)
    void nextTick(() => {
      expandedWrapperRef.value?.removeAttribute('style')
    })
  } else {
    emit('dragstart', event)
  }
}
</script>

<template>
  <div ref="containerRef" class="filter-element" :class="{ expanded }">
    <div class="filter-info">
      <div class="drag-area" @mousedown="handleDragStart($event)">
        <DragIndicator />
      </div>
      <div class="icon">
        <FilterIcon :filter="filter" />
      </div>
      <span class="filter-name">
        {{ $t(`management.courses.filters.types.${filter}`) }}
      </span>
      <div v-if="selected.length" class="filter-entities">
        <span class="filter-mode">
          <MingcuteExcludeFill v-if="exclude" data-test="include-indicator" />
          <MingcuteIntersectFill v-else data-test="exclude-indicator" />
        </span>
        <span class="filter-entity-count">
          {{ firstSelected }}
          <template v-if="selected.length > 1"> +{{ selected.length - 1 }}</template>
        </span>
      </div>
      <button class="expand" @click="handleToggleExpanded">
        <Expand />
      </button>
      <button class="delete" @click="emit('delete')">
        <Remove />
      </button>
    </div>
    <div ref="expandedWrapperRef" class="filter-details-wrapper">
      <div v-if="expanded" class="filter-details-container">
        <div class="filter-details">
          <div class="filter-mode-selector">
            <label>
              {{ $t('management.courses.filters.mode') }}
            </label>
            <!-- TODO: use a proper toggle button -->
            <ButtonGroup>
              <Button data-test="include-btn" icon-button @click="exclude = false">
                <MingcuteIntersectFill />
              </Button>
              <Button data-test="exclude-btn" icon-button @click="exclude = true">
                <MingcuteExcludeFill />
              </Button>
            </ButtonGroup>
          </div>
          <div class="filter-entities-selector">
            <TagsInputCombobox
              v-model="selected"
              :options="entities.map((e) => ({ value: e.id, name: e.name, alias: e.alias }))"
              :placeholder="$t('form.searchable.search')"
            />
          </div>
          <div class="filter-actions">
            <Button data-test="expanded-delete" :color="Color.RED" :look="Look.OUTLINED" @click="emit('delete')">
              {{ $t('management.courses.filters.delete') }}
            </Button>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped>
.filter-element {
  display: flex;
  flex-direction: column;
  background-color: #eee;
  padding: 0.5rem 0.5rem 0;
  border-radius: 4px;
  position: relative;
}

.filter-info {
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.expand,
.delete {
  all: unset;
  box-sizing: border-box;
  cursor: pointer;
}

.expand {
  transition: transform 0.2s;
}

.drag-area,
.icon,
.expand,
.delete,
.filter-mode {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 1.5rem;
  height: 1.5rem;
  flex-shrink: 0;
}

.delete {
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(35%, -35%);
  background-color: var(--color-red);
  width: 1.25rem;
  height: 1.25rem;
  border-radius: 50%;
  transition: opacity 0.2s;
  color: white;

  pointer-events: none;
  opacity: 0;
}

.filter-element:hover:not(.expanded) .delete {
  pointer-events: auto;
  opacity: 1;
}

.drag-area {
  cursor: move;
  color: var(--text-muted);
}

.filter-name {
  flex: 1;
}

.filter-details-wrapper {
  margin-top: 0.5rem;
  overflow: hidden;
  transition: height 0.3s;
  height: 0;
}

.filter-entities {
  display: flex;
  align-items: center;
  gap: 0.125rem;
  font-size: 0.8rem;
  line-height: 1rem;
}

.filter-mode {
  width: 1.2rem;
  height: 1.2rem;
}

.filter-details {
  padding: 0.5rem;
  /* TODO: adjust colors with flop's theme + dark theme */
  border-top: 1px #ddd solid;
  display: flex;
  flex-direction: column;
  gap: 1rem;
}

.filter-mode-selector {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.filter-actions {
  display: flex;
  justify-content: flex-end;
  align-items: center;
  gap: 0.5rem;
}

.expand svg,
.delete svg,
.drag-area svg {
  width: 100%;
  height: 100%;
  fill: currentColor;
}

/* Expanded */
.expanded .expand {
  transform: rotate(180deg);
}
</style>
