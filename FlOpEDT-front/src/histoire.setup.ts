import i18n from '@/i18n'
import { createPinia } from 'pinia'
import { defineSetupVue3 } from '@histoire/plugin-vue'
import { Quasar } from 'quasar'

import './globals.css'
import './histoire-global.css'

export const setupVue3 = defineSetupVue3(({ app }) => {
  const pinia = createPinia()
  app.use(pinia)
  app.use(i18n)
  app.use(Quasar, {
    plugins: {},
  })
})
