import { Course, CourseCreateData, CourseFlatAPI, CourseUpdateData, Department, User } from '@/ts/type'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { ref } from 'vue'

import { api } from '@/utils/api'
import { useCourseTypeStore } from '@/stores/timetable/courseType'
import { useRoomStore } from '@/stores/timetable/room'
import { usePeriodStore } from '@/stores/timetable/period'
import { useGroupStore } from '@/stores/timetable/group'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useCourseModuleStore } from '@/stores/timetable/courseModule'

export const useCourseStore = defineStore('courses', () => {
  const isLoading = ref(false)
  const error = ref<Error>()
  const courses = ref<Course[]>([])

  // This is ugly but ugh don't really have a choice here
  const courseTypeStore = useCourseTypeStore()
  const roomTypeStore = useRoomStore()
  const periodStore = usePeriodStore()
  const groupsStore = useGroupStore()
  const tutorStore = useTutorStore()
  const moduleStore = useCourseModuleStore()
  const toTutor = (u: User) => ({
    id: u.id,
    username: u.username,
    first_name: u.firstname,
    last_name: u.lastname,
    email: u.email,
  })
  function courseFlatToCourse(course: CourseFlatAPI): Course {
    return {
      id: course.id,
      type: {
        ...courseTypeStore.courseTypes.find((t) => t.id === course.type_id)!,
        department: new Department(),
      },
      duration: course.duration,
      room_type: roomTypeStore.roomsFetched.find((r) => r.id === course.room_type_id)!,
      period: course.period_id !== null ? periodStore.periods.find((p) => p.id === course.period_id)! : null,
      groups: course.groups.map(({ id }) => ({
        ...groupsStore.groups.find((g) => g.id === id)!,
        is_structural: false,
        train_prog: '',
      })),
      no: 0,
      tutor: course.tutor_id !== null ? toTutor(tutorStore.tutors.find((t) => t.id === course.tutor_id)!) : null,
      supp_tutors: course.supp_tutors.map(({ id }) => toTutor(tutorStore.tutors.find((t) => t.id === id)!)),
      module: {
        ...moduleStore.courseModules.find((m) => m.id === course.module_id)!,
        display: { color_bg: '', color_txt: '' },
      },
      modulesupp: { abbrev: '' },
      pay_module: { abbrev: '' },
      is_graded: course.is_graded,
    }
  }

  async function fetchAllCourses(dept: string) {
    isLoading.value = true
    courses.value = []
    error.value = void 0

    try {
      courses.value = await api.fetch.courses({ department: dept })
    } catch (e) {
      if (!(e instanceof Error)) {
        error.value = new Error('Unrecognized error: ' + e?.toString())
      } else {
        error.value = e
      }
    } finally {
      isLoading.value = false
    }
  }

  async function updateCourse(courseUpdateData: CourseUpdateData): Promise<Course> {
    const actualCourseIndex = courses.value.findIndex((element) => element.id === courseUpdateData.id)
    if (actualCourseIndex === -1) throw new Error('Course not found')

    const updatedCourseFlat = await api.patch.patchCourse(courseUpdateData)
    const updatedCourse = courseFlatToCourse(updatedCourseFlat)
    Object.assign(courses.value[actualCourseIndex], updatedCourse)
    return updatedCourse
  }

  async function postCourse(courseData: CourseCreateData): Promise<Course> {
    const createdCourseFlat = await api.post.postCourse(courseData)
    const createdCourse = courseFlatToCourse(createdCourseFlat)
    courses.value.push(createdCourse)
    return createdCourse
  }

  async function deleteCourse(courseToDelete: Course): Promise<void> {
    await api.delete.deleteCourse(courseToDelete.id)
    const index = courses.value.indexOf(courseToDelete, 0)
    if (index > -1) {
      courses.value.splice(index, 1)
    }
  }

  return {
    isLoading,
    error,
    courses,
    fetchAllCourses,
    updateCourse,
    postCourse,
    deleteCourse,
  }
})

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useCourseStore, import.meta.hot))
}
