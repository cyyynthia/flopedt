/** The 'period.ts' file defines a store for managing periods in the application.

 The 'usePeriodStore' function is exported which, when invoked, returns an instance of the store.

 The store contains the following state:
 - 'periods': an array of periods.
 - 'isAllPeriodsFetched': a boolean flag indicating whether all periods have been fetched.

 The store also contains a computed property:
 - 'getPeriodById': a function that takes a period ID and returns the corresponding period object.

 The store provides the following actions:
 - 'fetchPeriods': an asynchronous function that fetches periods from the API and updates the 'periods' state.
 - 'clearPeriods': a function that clears the 'periods' state and resets 'isAllPeriodsFetched' to false.
*/

import { ref } from 'vue'
import { api } from '@/utils/api'
import { defineStore } from 'pinia'
import { Period } from '@/ts/type'

export const usePeriodStore = defineStore('period', () => {
  const periods = ref<Period[]>([])
  const isAllPeriodsFetched = ref(false)

  async function fetchPeriods(): Promise<void> {
    await api.getNewPeriods().then((result: Array<Period>) => {
      periods.value = result
      isAllPeriodsFetched.value = true
    })
  }

  function clearPeriods(): void {
    periods.value = []
    isAllPeriodsFetched.value = false
  }

  return {
    periods,
    isAllPeriodsFetched,
    fetchPeriods,
    clearPeriods,
  }
})
