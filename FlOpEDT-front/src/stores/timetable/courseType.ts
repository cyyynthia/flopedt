import { CourseType } from '@/ts/type'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { ref } from 'vue'

import { api } from '@/utils/api'

export const useCourseTypeStore = defineStore('courseType', () => {
  const isLoading = ref(false)
  const error = ref<Error>()
  const courseTypes = ref<CourseType[]>([])

  async function fetchAllCourseTypes(dept: string) {
    isLoading.value = true
    courseTypes.value = []
    error.value = void 0

    try {
      courseTypes.value = await api.fetch.courseTypes({ department: dept })
    } catch (e) {
      if (!(e instanceof Error)) {
        error.value = new Error('Unrecognized error: ' + e?.toString())
      } else {
        error.value = e
      }
    } finally {
      isLoading.value = false
    }
  }

  return {
    isLoading,
    error,
    courseTypes,
    fetchAllCourseTypes,
  }
})

export function getCourseType() {
  const courseTypeStore = useCourseTypeStore()
  return courseTypeStore.courseTypes.map((courseType) => courseType.name)
}

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useCourseTypeStore, import.meta.hot))
}
