import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { Quasar } from 'quasar'

import router from '@/router'
import i18n from '@/i18n'

import App from './App.vue'

import './style.css'

createApp(App)
  .use(router)
  .use(createPinia())
  .use(i18n)
  .use(Quasar, {
    plugins: {},
  })
  .mount('#app')
