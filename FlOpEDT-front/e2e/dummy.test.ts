import { test, expect } from '@playwright/test'

test('it works', async ({ page }) => {
  await page.goto('/')
  await expect(page.getByRole('link', { name: 'flop!EDT' })).toBeVisible()

  await page.goto('/fr/edt/default')
  await expect(page.getByRole('link', { name: 'flop!EDT' })).toBeVisible()
})
